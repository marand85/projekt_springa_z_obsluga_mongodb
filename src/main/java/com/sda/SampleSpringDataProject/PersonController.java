package com.sda.SampleSpringDataProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    private PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createPerson(@RequestBody CreatePersonRequest request) {
        personService.createPerson(request);
    }

    @GetMapping("/all")
    public Page<Person> findAllPersons(@PageableDefault(size = 2) Pageable pageable) {
        return personService.getAllPersons(pageable);
    }

    @GetMapping("/search")
    public List<Person> findByFirstName(@RequestParam String firstName){
        return  personService.findPersonsByFirstName(firstName);
    }

    @GetMapping("/findByAge")
    public List<Person> findByAge(@RequestParam Integer min, @RequestParam Integer max){
        return personService.findPersonsByAge(min, max);
    }
}
