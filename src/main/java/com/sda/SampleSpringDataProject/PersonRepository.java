package com.sda.SampleSpringDataProject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PersonRepository extends MongoRepository<Person, String> {

    List<Person> findAllByFirstName(String firstName);

    List<Person> findAllByAgeBetween(Integer min, Integer max);

    Page<Person> findAllBy(Pageable pageable);

}
