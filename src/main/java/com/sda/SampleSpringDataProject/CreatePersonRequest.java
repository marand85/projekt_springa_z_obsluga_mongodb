package com.sda.SampleSpringDataProject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@AllArgsConstructor
public class CreatePersonRequest {

    private String firstName;

    private String lastName;

    private Integer age;

    private String city;

}
