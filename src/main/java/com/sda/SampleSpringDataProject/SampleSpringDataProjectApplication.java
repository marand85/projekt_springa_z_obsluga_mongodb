package com.sda.SampleSpringDataProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSpringDataProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleSpringDataProjectApplication.class, args);
	}
}
