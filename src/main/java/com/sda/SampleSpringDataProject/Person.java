package com.sda.SampleSpringDataProject;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "persons")
@Data
@AllArgsConstructor
@Builder
public class Person {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private Integer age;

    private String city;

}
