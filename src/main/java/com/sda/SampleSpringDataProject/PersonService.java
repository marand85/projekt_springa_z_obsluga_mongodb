package com.sda.SampleSpringDataProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    private PersonRepository repository;

    @Autowired
    public PersonService(PersonRepository repository) {
        this.repository = repository;
    }

    public void createPerson(CreatePersonRequest request) {
        Person person = Person.builder()
                .age(request.getAge())
                .city(request.getCity())
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .build();

        repository.save(person);
    }

    public Page<Person> getAllPersons(Pageable pageable) {
        return repository.findAllBy(pageable);
    }

    public List<Person> findPersonsByFirstName(String firstName) {
        return repository.findAllByFirstName(firstName);
    }

    public List<Person> findPersonsByAge(Integer min, Integer max) {
        return repository.findAllByAgeBetween(min, max);
    }
}
